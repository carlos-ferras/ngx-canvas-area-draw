import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import 'hammerjs';
import { NgxCanvasAreaDrawModule } from 'ngx-canvas-area-draw';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

export function windowFactory() {
  return window;
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgxCanvasAreaDrawModule
  ],
  providers: [
    {
      provide: 'window',
      useFactory: windowFactory
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
