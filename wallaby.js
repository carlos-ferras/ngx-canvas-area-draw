module.exports = function(wallaby) {
  const jestTransform = (file) =>
    require('jest-preset-angular/preprocessor').process(file.content, file.path, {
      globals: {
        __TRANSFORM_HTML__: true
      },
      rootDir: __dirname
    });

  return {
    files: [
      { pattern: 'package.json', load: false },
      { pattern: 'tsconfig.json', load: false },
      { pattern: 'src/tsconfig.spec.json', load: false },
      { pattern: 'src/jest-setup.ts', load: false },
      { pattern: 'jest-global-mocks.ts', load: false },
      { pattern: 'src/**/*.spec.ts', ignore: true },
      { pattern: 'babel.config.js', instrument: false },
      'src/**/*.+(ts|html|json|css|scss|jpg|jpeg|gif|png|svg)'
    ],

    filesWithNoCoverageCalculated: [
      'src/main.ts',
      'src/polyfills.ts',
      'src/wallabyTest.ts',
      'src/jest-global-mocks.ts',
      'src/**/index.ts',
      'src/**/*.module.ts',
      'src/app/test-utils/**/*'
    ],

    tests: ['src/app/**/*.spec.ts'],

    env: {
      runner: 'node',
      type: 'node'
    },

    compilers: {
      '**/*.html': (file) => ({
        code: jestTransform(file),
        map: {
          mappings: [],
          names: [],
          sources: [],
          version: 3
        },
        ranges: []
      })
    },

    preprocessors: {
      'src/**/*.js': [
        jestTransform,
        (file) =>
          require('@babel/core').transform(file.content, {
            compact: false,
            filename: file.path,
            presets: [require('babel-preset-jest')],
            sourceMap: true
          })
      ]
    },

    testFramework: 'jest',

    debug: true
  };
};
