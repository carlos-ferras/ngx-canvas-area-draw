// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');
let path = require('path');

exports.config = {
  allScriptsTimeout: 11000,
  baseUrl: 'http://localhost:4200/',
  beforeLaunch: function() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  },
  capabilities: {
    browserName: 'chrome'
  },
  directConnect: true,
  framework: 'jasmine',
  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000,
    print: function() {},
    showColors: true
  },
  specs: ['./src/**/*.e2e-spec.ts'],
  onPrepare() {
    let jasmineReporters = require('jasmine-reporters');
    let junitReporter = new jasmineReporters.JUnitXmlReporter({
      consolidateAll: true,
      filePrefix: 'e2e_report',
      savePath: 'test_results'
    });
    jasmine.getEnv().addReporter(junitReporter);
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};
