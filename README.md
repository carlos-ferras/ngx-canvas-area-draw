# ngx-canvas-area-draw [![pipeline status](https://gitlab.com/carlos-ferras/ngx-canvas-area-draw/badges/master/pipeline.svg)](https://gitlab.com/carlos-ferras/ngx-canvas-area-draw/commits/master) [![coverage report](https://gitlab.com/carlos-ferras/ngx-canvas-area-draw/badges/master/coverage.svg)](https://gitlab.com/carlos-ferras/ngx-canvas-area-draw/commits/master)

Angular v7 Directive to draw polygons over an image using canvas

