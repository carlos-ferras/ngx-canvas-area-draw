# ngx-canvas-area-draw

Angular v7 Directive to draw polygons over an image using canvas

### Mandatory:

- Install Node.js v10.15.2 and npm v6.9.0

  > To easily install and manage versions of node and npm is recommended to use [nvm](https://github.com/creationix/nvm).

  1. Install nvm:  
     `curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash`
  2. Reload the shell:  
     `source ~/.bashrc`
  3. Verify that nvm has been installed:  
     `command -v nvm`
  4. List Node.js versions installed:  
     `nvm ls`
  5. Install Node.js v10.15.2 and npm v6.9.0:  
     `nvm install v10.15.2`
  6. Check the Node.js versions installed again:  
     `nvm ls`

- After install node and npm, in the project root run this command:

  `npm install`

## Documentation

- To create the documentation we use [compodoc](https://compodoc.app/guides/getting-started.html).

- To generate the documentation execute this commands in the project root:

  `npm run docs:generate`

- To run the documentation execute this commands in the project root:

  `npm run docs:serve`
  
## Unit tests

- To execute the unit test run in the terminal `npm run test` || `npm run test:coverage`

- You will find the tests coverage report in the ./coverage/index.html file.

## SonarQube Scan

- Export this environment variable

  * **TS_SONAR_URL**

## Build

- For the library run `npm install && npm run build`

- For the demo run `npm install && npm run build:demo`

## Deployment

- Run `npm run build && cd dist/ngx-canvas-area-draw && npm publish`
